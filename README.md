# Amazon SageMaker Emotion Detection Model

## Project Summary

 As we benefit from integrating technology into our everyday lives, we still struggle to make meaningful relationships with those around us. I find that screen time can often get in the way of having meaningful conversations with my teenagers and pre-teen! My newest project uses Amazon [SageMaker](https://aws.amazon.com/sagemaker/) to develop an emotion detection machine learning model that uses computer vision to help us better connect and respond to the emotional state of others.

The aim of this project is to use Amazon SageMaker’s [Image Classification](https://docs.aws.amazon.com/sagemaker/latest/dg/image-classification.html) algorithm to classify images of my daughter in four emotional states: happy, angry, sad, and neutral. For this illustration, supervised learning that supports multi-label classification is used. The training data consists of images of my daughter in each emotional state. The images were converted to grayscale and loaded to [Amazon S3](https://aws.amazon.com/s3/) in order to train the model.

The end result of this project is a notification sent to me by the [Amazon Simple Notification Service (SNS)](https://aws.amazon.com/sns/) that contains her emotional state. I am notified as soon as she arrives home from school, when the AWS DeepLens camera spots her. Before I get home, I know if she's happy, angry, sad, or feeling blah (i.e. neutral).  If she's sad, I will bring home her favorite treat, a milkshake from Chick-fil-A! If she's angry, I call her immediately to find out why she's so upset. If she's happy, I will quietly smile on the inside knowing she's had a great day and that all is right with the world!

## Architecture Diagram
![alt text](https://child-emotion-detection.s3.amazonaws.com/SageMaker+Emotion+Prediction.png "SageMaker Archi Diagram")

| Component     | Detail        | 
| ------------- |:-------------:| 
| AWS DeepLens   | A deep learning-enabled video camera used to watch a scene. |
| S3        | An object storage service used to hold pictures taken by AWS DeepLens.      |
| Lambda        | Lambda function authored in Python that is triggered when a photo of my daughter is uploaded to S3.      |
| SageMaker     | Machine Learning service used to train and host the emotion detection model.|
| Simple Notification Service     | A service used to notify me of my daughter's emotional state. |

### Overview
The emotion detection process is kicked off when [AWS DeepLens](https://aws.amazon.com/deeplens/), a deep learning-enabled video camera, idenfities my daughter in a scene it's watching. Once AWS DeepLens identifies my daughter, it loads her photo to Amazon S3. The load to S3 kicks off a Lambda that retrieves the photo and sends it to the image classification model to determine her emotional state. The prediction of her emotional state is emailed to me by Amazon Simple Notification Service.

## Sample Prediction
The model returns an array of confidence scores for each emotional state based on a give image.

The sample prediction below is one that indicates **anger**. Each number represents a probability score for each emotional state: ['happy', 'sad', 'angry', 'neutral'].

[0.2040698230266571, 0.030062051489949226, 0.7600065469741821, 0.0058615305460989475]

![alt text](https://child-emotion-detection.s3.amazonaws.com/notification.png "Notification")

## Learnings
* I considered using Amazon Rekognition because it offers a simple API that provides facial recognition and sentiment analysis; however, I chose Amazon Sagemaker because I wanted a custom image classification model that was trained **specifically** on my daughter.
* AWS DeepLens should be placed in an area with generous natural light. If the surroundings are too dark, the face detection model cannot identify faces in the scene.

## Next Steps
* I'd like to consider the use of Transfer Learning to determine if it can speed up the training the process.
* I plan to include more images of my daughter (at different angles) to increase the prediction accuracy.

## Author
[Kesha Williams](https://www.linkedin.com/in/java-rock-star-kesha/)
