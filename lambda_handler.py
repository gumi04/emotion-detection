import boto3
import json
import os

# grab environment variables
ENDPOINT_NAME = os.environ['ENDPOINT_NAME']

runtime = boto3.Session().client(service_name='sagemaker-runtime', region_name='us-east-1')
s3 = boto3.client('s3')
sns = boto3.client('sns')


def lambda_handler(event, context):
    # set the image categories
    image_categories = ['happy', 'sad', 'angry', 'neutral']

    # parse the trigger event for the bucket name and object key (i.e. image file name)
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = event['Records'][0]['s3']['object']['key']

    # retrieve the image of my daughter
    image_obj = s3.get_object(Bucket=bucket, Key=key)
    image_contents = image_obj['Body'].read()

    # call model for predicting my daughter's emotional state
    response = runtime.invoke_endpoint(EndpointName=ENDPOINT_NAME,
                                       ContentType='application/x-image',
                                       Body=bytearray(image_contents))

    # read the prediction result and parse the json
    result = response['Body'].read()
    result = json.loads(result)
    message = "Your daughter is "

    # the index of the emotion with the highest probability score
    index = result.index(max(result))

    publish_message(message + image_categories[index])

    return message


def publish_message(prediction):
    print(prediction)
    sns.publish(
        TopicArn='arn:aws:sns:us-east-1:0000000000:sns-topic-name-here',
        Message=json.dumps("The prediction is : " + prediction),
        MessageStructure='string',
        MessageAttributes={
            'summary': {
                'StringValue': 'Emotion Prediction',
                'DataType': 'String'
            }
        }
    )
